/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'

import { StyleContext } from '../context/StyleProvider'

const Input = ({ margin, border, placeholder, name, onChange, value }) => {
  const { style } = useContext(StyleContext)

  const inputStyle = {
    margin,
    display: 'block',
    padding: '.5rem 1rem',
    borderRadius: '6px',
    border
  }

  return (
    <input
      className={style}
      css={inputStyle}
      placeholder={placeholder}
      name={name}
      onChange={onChange}
      value={value}
    />
  )
}

export default Input

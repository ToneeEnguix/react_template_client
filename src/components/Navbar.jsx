/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import { NavLink } from 'react-router-dom'
import { StyleContext } from '../context/StyleProvider'
import sun from '../resources/media/sun.gif'
import moon from '../resources/media/moon.gif'
import userImg from '../resources/media/user.png'
import { AuthContext } from '../context/AuthProvider'

export default function Navbar () {
  const { style, setStyle } = useContext(StyleContext)
  const { isLoggedIn } = useContext(AuthContext)

  const mainStyle = {
    height: '50px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 5%',
    '.active': {
      color: style === 'light' ? 'darkblue' : 'lightblue'
    },
    a: {
      // fontSize: '18px'
    }
  }

  return (
    <div css={mainStyle}>
      <NavLink
        to='/home'
        className={({ isActive }) => (isActive ? 'active' : undefined)}
      >
        Home
      </NavLink>
      <div css={rightPart}>
        <button
          css={modeSelectorWrapper}
          className='pointer'
          onClick={() => setStyle(style === 'light' ? 'dark' : 'light')}
        >
          {style === 'light' ? (
            <img alt='sun' src={sun} />
          ) : (
            <img className='invert' alt='moon' src={moon} />
          )}
        </button>
        <div className='userWrapper'>
          <NavLink className='flex' to={isLoggedIn ? '/profile' : '/login'}>
            <img
              className={style === 'dark' ? 'invert' : ''}
              alt='user'
              src={userImg}
            />
          </NavLink>
        </div>
      </div>
    </div>
  )
}

const rightPart = {
  display: 'flex',
  alignItems: 'center',
  '.userWrapper': {
    margin: '7px 0 0',
    img: {
      width: '30px'
    }
  }
}

const modeSelectorWrapper = {
  width: 'fit-content',
  display: 'flex',
  margin: '8px 1rem 0 0',
  img: {
    height: '38px',
    width: '38px'
  }
}

/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import { StyleContext } from '../context/StyleProvider'

const Button = ({
  text,
  onClick,
  color = 'black',
  backgroundColor = 'white',
  margin,
  backgroundImage,
  border,
  borderRadius,
  bgColorHover,
  colorHover
}) => {
  const { style } = useContext(StyleContext)

  const buttonStyle = {
    color,
    backgroundColor,
    margin,
    backgroundImage,
    padding: '0.5rem 1rem',
    borderRadius: borderRadius || '6px',
    border,
    ':hover': {
      color: colorHover || backgroundColor,
      backgroundColor: bgColorHover || color,
      border: border ? '1px solid white' : 'none'
    }
  }

  return (
    <button className={style} css={buttonStyle} onClick={onClick}>
      {text}
    </button>
  )
}

export default Button

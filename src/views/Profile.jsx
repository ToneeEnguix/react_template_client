/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import WithToast from '../modals/Toast'
import WithModal from '../modals/YesNo'
import { URL } from '../config'
import Button from '../components/Button'
import { AuthContext } from '../context/AuthProvider'
import { StyleContext } from '../context/StyleProvider'

function ProfileComp ({ setMessage, setModalContent }) {
  const { user, logout } = useContext(AuthContext)
  const { style } = useContext(StyleContext)

  const deleteUser = async () => {
    try {
      const res = await axios.delete(`${URL}/user/delete/${user.email}`)
      setMessage({ ok: res.data.ok, txt: res.data.body })
      res.data.ok && setTimeout(() => logout(), 1500)
    } catch (err) {
      console.error(err)
    }
  }

  const openModal = () => {
    setModalContent({
      openModal: true,
      text: 'Are you sure you want to delete your account?',
      onClick1: deleteUser
    })
  }

  return (
    <div css={mainStyle}>
      <h1>Profile</h1>
      <h2>Hello, {user.email}</h2>
      <Link to='/change_password' className='change_pw'>
        Change password
      </Link>
      <Button
        text='Log Out'
        onClick={logout}
        margin='1rem 0 0'
        border={`1px solid ${style === 'light' ? 'black' : 'white'}`}
        bgColorHover={`${style === 'light' ? 'black' : 'white'}`}
        backgroundColor={`${style === 'light' ? 'white' : 'black'}`}
        color={`${style === 'light' ? 'black' : 'white'}`}
      />
      <Button
        text='Delete Account'
        onClick={openModal}
        margin='2rem 0 0'
        border='1px solid rgb(240, 10, 10)'
        borderRadius='0'
        color={`${style === 'light' ? 'black' : 'white'}`}
        backgroundColor={`${style === 'light' ? 'white' : 'black'}`}
        bgColorHover='rgb(240, 10, 10)'
        colorHover='white'
      />
    </div>
  )
}

const mainStyle = {
  minHeight: 'calc(100vh - 50px)',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '4rem',
  h1: { fontSize: '1.8rem' },
  h2: { fontSize: '1.2rem', marginTop: '4rem' },
  '.change_pw': {
    margin: '2rem 0',
    textDecoration: 'underline'
  }
}

// using this components to use modals
export default function Profile () {
  return (
    <WithToast>
      <WithModal>
        <ProfileComp />
      </WithModal>
    </WithToast>
  )
}

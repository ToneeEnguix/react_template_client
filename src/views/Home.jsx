/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import { AuthContext } from '../context/AuthProvider'

export default function Home () {
  const { user, isLoggedIn } = useContext(AuthContext)

  return (
    <div css={mainStyle} className='flexCenter'>
      {isLoggedIn && (
        <div className='flexCenter'>
          <h1>Hello, {user.email}</h1>
        </div>
      )}
    </div>
  )
}

const mainStyle = {
  width: '100%',
  minHeight: 'calc(100vh - 50px)'
}

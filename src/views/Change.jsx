/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

import { URL } from '../config'
import Input from '../components/Input'
import Button from '../components/Button'
import WithToast from '../modals/Toast'
import { StyleContext } from '../context/StyleProvider'

const ChangeComp = ({ setMessage }) => {
  const { style } = useContext(StyleContext)
  const navigate = useNavigate()

  const [form, setForm] = useState({
    old_pw: '',
    new_pw: '',
    repeat_pw: ''
  })

  const writeForm = e => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const submitChange = async e => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/user/change_pw`, {
        old_pw: form.old_pw,
        new_pw: form.new_pw,
        repeat_pw: form.repeat_pw
      })
      if (!res.data.ok)
        return setMessage({ ok: res.data.ok, txt: res.data.body })
      setMessage({ ok: res.data.ok, txt: res.data.body })
      setTimeout(() => {
        navigate(-1)
      }, 1500)
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <form css={mainStyle} className='flexCenter' onSubmit={submitChange}>
      <h1>Change password</h1>
      <Input
        placeholder='Old password'
        name='old_pw'
        onChange={writeForm}
        margin='1rem 0 0 0'
        border='1px solid gray'
        value={form.old_pw}
      />
      <Input
        placeholder='New password'
        name='new_pw'
        onChange={writeForm}
        margin='1rem 0 0 0'
        border='1px solid gray'
        value={form.new_pw}
      />
      <Input
        placeholder='Repeat password'
        name='repeat_pw'
        onChange={writeForm}
        margin='1rem 0 0 0'
        border='1px solid gray'
        value={form.repeat_pw}
      />
      <Button
        text='Confirm change'
        margin='1rem 0 0 0'
        border={`1px solid ${style === 'light' ? 'black' : 'white'}`}
        bgColorHover={`${style === 'light' ? 'black' : 'white'}`}
        backgroundColor={`${style === 'light' ? 'white' : 'black'}`}
        color={`${style === 'light' ? 'black' : 'white'}`}
      />
    </form>
  )
}

const mainStyle = {
  minHeight: 'calc(100vh - 50px)',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'left',
  marginTop: '4rem',
  h1: { fontSize: '1.8rem', marginBottom: '2rem' },
  button: { marginTop: '2rem' }
}

// using this components to use modals
export default function Change () {
  return (
    <WithToast>
      <ChangeComp />
    </WithToast>
  )
}

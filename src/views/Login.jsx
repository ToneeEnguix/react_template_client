/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import { useContext, useState } from 'react'
import axios from 'axios'

import WithToast from '../modals/Toast'
import { AuthContext } from '../context/AuthProvider'
import { URL } from '../config'
import Button from '../components/Button'
import Input from '../components/Input'

// this file exports the component at the end, below
function LoginComp ({ setMessage }) {
  const { login } = useContext(AuthContext)

  const [form, setForm] = useState({
    email: '',
    password: ''
  })

  const writeForm = e => {
    setForm({
      ...form,
      [e.target.name]:
        e.target.name === 'email'
          ? e.target.value.toLowerCase()
          : e.target.value
    })
  }

  const submitForm = async e => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/user/login`, {
        email: form.email,
        password: form.password
      })
      if (!res.data.ok)
        return setMessage({ ok: res.data.ok, txt: res.data.body })
      setTimeout(() => login(res.data.body), 1000)
      setMessage({
        ok: res.data.ok,
        txt: `Welcome back, ${res.data.body.email}`
      })
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <form css={formStyle} onSubmit={submitForm}>
      <label>LOG IN</label>
      <Input
        placeholder='Email'
        name='email'
        onChange={writeForm}
        margin='1rem 0 0 0'
        border='1px solid gray'
        value={form.email}
      />
      <Input
        placeholder='Password'
        name='password'
        onChange={writeForm}
        margin='1rem 0 0 0'
        border='1px solid gray'
        value={form.password}
      />
      <Button
        text='Log In'
        backgroundImage='linear-gradient(to right, rgb(1 134 218), rgb(182 49 167))'
        color='white'
        margin='1rem 0 0 0'
      />
      <Link to='/signup'>Sign Up</Link>
      <Link to='/forgot_pw'>Forgot Password</Link>
    </form>
  )
}

const formStyle = {
  minHeight: 'calc(100vh - 50px)',
  paddingTop: '5rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  label: { textTransform: 'uppercase', fontSize: '18px' },
  a: {
    marginTop: '1rem',
    textDecoration: 'underline',
    fontSize: '.9rem'
  }
}

// using this components to use modals
export default function Signup () {
  return (
    <WithToast>
      <LoginComp />
    </WithToast>
  )
}

/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState } from 'react'
import axios from 'axios'
import WithToast from '../modals/Toast'
import { useNavigate } from 'react-router-dom'

import { URL } from '../config'
import Input from '../components/Input'
import Button from '../components/Button'
import { Link } from 'react-router-dom'

const ForgotComp = ({ setMessage }) => {
  const navigate = useNavigate()

  const [email, setEmail] = useState('')

  return (
    <div css={mainStyle}>
      <div className='flexCenter' style={{ height: '50px' }}>
        <img
          alt='back'
          src='https://img.icons8.com/ios-filled/50/null/left.png'
          className='backStyle pointer'
          onClick={() => navigate(-1)}
        />
        <label>Forgot Password</label>
      </div>
      <Input
        placeholder='Enter your email'
        name='email'
        onChange={e => setEmail(e.target.value)}
        margin='1rem 0 0 0'
        border='1px solid gray'
        value={email}
      />
    </div>
  )
}

const mainStyle = {
  width: '100%',
  flexDirection: 'column',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '4rem',
  label: { textTransform: 'uppercase', fontSize: '18px', marginRight: '25px' },
  '.backStyle': {
    width: '20px',
    marginRight: '25px',
    ':hover': {
      width: '25px',
      marginRight: '20px'
    }
  }
}

export default function Forgot () {
  return (
    <WithToast>
      <ForgotComp />
    </WithToast>
  )
}

/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import { useContext, useState } from 'react'
import axios from 'axios'

import WithToast from '../modals/Toast'
import { StyleContext } from '../context/StyleProvider'
import { AuthContext } from '../context/AuthProvider'
import { URL } from '../config'
import Input from '../components/Input'
import Button from '../components/Button'

// this file exports the component at the end, below
function SignupComp ({ setMessage }) {
  const { style } = useContext(StyleContext)
  const { login } = useContext(AuthContext)

  const [form, setForm] = useState({
    email: '',
    password: '',
    confirmPassword: ''
  })

  const writeForm = e => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const submitForm = async e => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/user/signup`, {
        email: form.email,
        password: form.password,
        confirmPassword: form.confirmPassword
      })
      if (!res.data.ok)
        return setMessage({ ok: res.data.ok, txt: res.data.body })
      setTimeout(() => login(res.data.body), 1000)
      setMessage({
        ok: res.data.ok,
        txt: `Welcome, ${res.data.body.email}`
      })
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <form css={formStyle} onSubmit={submitForm}>
      <label>SIGN UP</label>
      <Input placeholder='Email' name='email' onChange={writeForm} />
      <Input placeholder='Password' name='password' onChange={writeForm} />
      <Input
        placeholder='Repeat Password'
        name='confirmPassword'
        onChange={writeForm}
      />
      <Button
        text='Sign Up'
        backgroundImage='linear-gradient(to right, rgb(1 134 218), rgb(182 49 167))'
        color='white'
        margin='1rem 0 0 0'
      />
      <Link to='/login'>Log In</Link>
    </form>
  )
}

const formStyle = {
  minHeight: 'calc(100vh - 50px)',
  paddingTop: '5rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  input: {
    marginTop: '1rem',
    display: 'block',
    padding: '.5rem 1rem',
    borderRadius: '6px',
    border: '1px solid lightgray'
  },
  a: {
    marginTop: '1rem',
    textDecoration: 'underline',
    fontSize: '.9rem'
  }
}

// using this components to use modals
export default function Signup () {
  return (
    <WithToast>
      <SignupComp />
    </WithToast>
  )
}

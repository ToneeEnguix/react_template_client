import { useState, createContext, useEffect } from 'react'
import Cookies from 'js-cookie'

export const StyleContext = createContext(null)

export const StyleProvider = ({ children }) => {
  const [style, setStyle] = useState(null)

  useEffect(() => {
    const preferred_style = Cookies.get('preferred_style')
    if (!preferred_style) setStyle('light')
    else setStyle(preferred_style)
  }, [])

  useEffect(() => {
    if (style !== null) Cookies.set('preferred_style', style)
  }, [style])

  return (
    <StyleContext.Provider value={{ style, setStyle }}>
      <div className={style}>{children}</div>
    </StyleContext.Provider>
  )
}

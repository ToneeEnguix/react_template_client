import React from 'react'
import { Navigate } from 'react-router-dom'
import { useContext } from 'react'

import { AuthContext } from '../context/AuthProvider'

const PublicRoute = ({ component: Component, restricted }) => {
  const { isLoggedIn } = useContext(AuthContext)

  return !isLoggedIn && !restricted ? <Component /> : <Navigate to='/home' />
}

export default PublicRoute

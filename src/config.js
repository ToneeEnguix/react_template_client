const localIP = '192.168.1.104'
const onlineIP = 'react-template-server.herokuapp.com'
const URL =
  window.location.hostname === `localhost`
    ? `http://${localIP}:4040`
    : `https://${onlineIP}`

export { URL }
